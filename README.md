[dietcoin-exp](https://gitlab.inria.fr/proman/dietcoin-exp) hosts the experimental environment code for the Dietcoin paper alongside its results.
It uses [dietcoin-lib](https://gitlab.inria.fr/proman/dietcoin-lib)  (forked from bitcoinj) as a support to build several programs for the experiments.


## Install

This repository requires java 8 and gradle:

    $ apt-get install openjdk-8-jdk openjdk-8-jre gradle

#### dietcoin-lib

To keep the library development separated from the experiments, dietcoin-lib is referenced as a git submodule in this project. Init it when cloning the repository:

    $ git submodule init

Once initialized, and each time the submodule is modified in a commit, update it:

    $ git submodule update

Alternatively, you can keep the submodule up to date with its upstream:

    $ git submodule update --remote

The subsequent builds made with gradle will recursively build the library.

#### Editing

Pre-configure the project for eclipse, then in eclipse File > Open projects from file system:

    $ gradle eclipse

#### Bitcoin-core server

The proxy requires a running bitcoin-core server to update its view of the chain.
Example of configuration file for the bitcoin-core server:

    $ cat ~/.bitcoin/bitcoin.conf
    disablewallet = 1
    # Ensures the proxy isn't banned because of his large number of requests
    whitelist = 127.0.0.1
    whitelist = CHANGEME # format x.x.x.x/y

You can refer to [this page](https://en.bitcoin.it/wiki/Running_Bitcoin#Command-line_arguments) of the bitcoin wiki for more config fields.

## Build and run

To remove what gradle built:

    $ gradle clean

To build the project and create a fat jar (that is easily exportable) with all the dependencies:

    $ gradle shadowjar

To run a step of an experiment:

    $ java -jar build/libs/dietcoin-exp.jar STEP [PROPERTY-FILE]
    Usage: dietcoin-exp STEP [PROPERTY-FILE]
        STEP      prepareIndex
                  prepareStoreBtc replayerBtc clientBtcFull clientBtcSpv
                  prepareStoreDtc replayerDtc clientDtcFull
        PROPERTY-FILE   Path to the experiment config file (default: debug.properties)

#### Experiment configuration file

All the configuration parameters (preparation, replayer, client) for an experiment are stored in one file formatted for java properties `PROPERTY-FILE`:

    $ cat debug.properties
    verbose = true

    btcHost = CHANGEME
    btcPort = 8333
    proxyHost = localhost
    proxyPort = 8334
    syncNonce = 16180

    headersDir = data/headers
    blockIndexFile = data/block_index.csv
    replayerBtcDir = data/replayer_btc
    replayerDtcDir = data/replayer_dtc
    maxStoredBlocks = 5
    bitsSharding = 3

    clientBtcSpvDir  = data/cli_btc_spv
    clientBtcFullDir = data/cli_btc_full
    clientDtcFullDir = data/cli_dtc_full
    clientMaxStoredBlocks = 4
    dietVerifySubchainLength = 3

    startHeight = 1000
    endHeight = 1010
    replayFreqMillis = 1000


#### Experiment steps

As everything is packed in one jar, the parameter describing which step to perform is specified in the `STEP` argument.
While the first instruction prepares for both bitcoin and dietcoin experiments, the following ones are specific to one protocol.

First use this parameter as `STEP`:

1. `prepareIndex`: creates a csv with all the pairs `(block height, block hash)` needed for the replayer

Then if you want to experiment with bitcoin, use these parameters in the given order:

2. `prepareStoreBtc`: creates a full node store up to block `startHeight` to bootstrap the replayer
3. `replayerBtc`: waits for a client to connect and replays blocks from `startHeight` to `endHeight`
4. `clientBtcSpv` or `clientBtcFull`: connects to the replayer and acts as a normal client performing an Initial Block Download up to block `startHeight-1` followed by a (sped-up) replay up to block `endHeight`

Or use these ones for dietcoin:

2. `prepareStoreDtc`
3. `replayerDtc`
4. `clientDtcFull`

---

## RPC configuration (optional)

RPC is optional and is only used for stats/debugging.

Activate the RPC interface on the bitcoin server:

    $ cat ~/.bitcoin/bitcoin.conf
    server = 1
    rpcallowip = 127.0.0.1
    rpcallowip = CHANGEME # format x.x.x.x/y
    rpcport = 8332
    rpcuser = CHANGEME
    rpcpassword = CHANGEME

Complete the property file of the proxy with matching fields:

    $ cat rpc.properties
    rpchost = CHANGEME
    rpcport = 8332
    rpcuser = CHANGEME
    rpcpassword = CHANGEME

Replace the main file from `build.gradle` with `rpc.MainRPC` and use the RPC as such:

    $ gradle clean
    $ gradle shadowjar
    $ java -jar build/libs/dietcoin-exp.jar rpc.properties
