### Must do
* Handle sigint when preparing the store
* Replayer: forward more than just blocks, see other inv messages
* Replayer: for full client, instead of storing all blocks, forward getdata(block) to the bitcoin-core server
