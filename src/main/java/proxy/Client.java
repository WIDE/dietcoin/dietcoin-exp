package proxy;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.bitcoinj.core.AbstractBlockChain;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.FullPrunedBlockChain;
import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Ping;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.core.listeners.NewBestBlockListener;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.LevelDBBlockStore;
import org.bitcoinj.store.LevelDBFullPrunedBlockStore;
import org.bitcoinj.wallet.Wallet;

import com.google.common.util.concurrent.SettableFuture;

import dietcoin.DietBlockChain;
import dietcoin.LevelDBShardedBlockStore;

public class Client {
    public static final int MODE_BTC_SPV  = 1;
    public static final int MODE_BTC_FULL = 2;
    public static final int MODE_DTC_DIET = 3;
    public static final int MODE_DTC_FULL = 4;

    public static void runClient(Properties prop, int mode, boolean withReplay) throws BlockStoreException, InterruptedException, ExecutionException, IOException {
        boolean verbose = Boolean.valueOf(prop.getProperty(Utils.PAR_VERBOSE, "false"));

        NetworkParameters params = MainNetParams.get();
        Context context = new Context(params, true); // Second argument is for Dietcoin (isInReplay)
        Wallet wallet = new Wallet(params);

        // The difference between clients is mostly in the pair (chain, store) that they use
        int nbBlocks = Integer.valueOf(prop.getProperty(Utils.PAR_CLIENT_MAX_STORED_BLOCKS));
        if (nbBlocks < 0) nbBlocks = Integer.MAX_VALUE;
        String dir = null;
        BlockStore store = null;
        AbstractBlockChain chain = null;
        switch (mode) {
        case MODE_BTC_SPV:
            dir = Utils.getUniqueDirName(prop.getProperty(Utils.PAR_CLIENT_BTC_SPV_DIR));
            store = new LevelDBBlockStore(context, new File(dir));
            chain = new BlockChain(context, store);
            break;
        case MODE_BTC_FULL:
            dir = Utils.getUniqueDirName(prop.getProperty(Utils.PAR_CLIENT_BTC_FULL_DIR));
            store = new LevelDBFullPrunedBlockStore(params, dir, nbBlocks);
            chain = new FullPrunedBlockChain(context, (LevelDBFullPrunedBlockStore) store);
            ((FullPrunedBlockChain) chain).setRunScripts(true); // Enable transaction verification
            break;
        case MODE_DTC_DIET:
            Utils.log("Unimplemented yet");
            break;
        case MODE_DTC_FULL:
            int verifySubchainLength = Integer.valueOf(prop.getProperty(Utils.PAR_DIET_VERIFY_SUBCHAIN_LENGTH, "0"));
            int nbBitsSharding = Integer.valueOf(prop.getProperty(Utils.PAR_NB_BITS_SHARDING));
            dir = Utils.getUniqueDirName(prop.getProperty(Utils.PAR_CLIENT_DTC_FULL_DIR));
            store = new LevelDBShardedBlockStore(params, dir, nbBlocks, nbBlocks); // Store as many versioned shards as blocks
            chain = new DietBlockChain(context, (LevelDBShardedBlockStore) store, verifySubchainLength);
            ((LevelDBShardedBlockStore) store).setShardingPolicy(nbBitsSharding);
            break;
        default:
            Utils.log("Incorrect replayer mode selected: " + mode);
            return;
        }

        PeerGroup group = new PeerGroup(context, chain);
        group.addWallet(wallet);
        group.setConnectTimeoutMillis(10*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);
        group.start();
        if (verbose) Utils.addPeerGroupEvents(group);

        // Connect to the proxy
        group.connectTo(new InetSocketAddress(prop.getProperty(Utils.PAR_PROXY_HOST), Integer.valueOf(prop.getProperty(Utils.PAR_PROXY_PORT))));
        Peer srv = group.waitForPeers(1).get().get(0);
        Utils.log("Downloading the up-to-date chain from the replayer at " + prop.getProperty(Utils.PAR_PROXY_HOST));

        // Initial block download: blocks in [1, startHeight-1]
        group.downloadBlockChain();

        int startHeight = Integer.valueOf(prop.getProperty(Utils.PAR_START_HEIGHT));
        int endHeight = Integer.valueOf(prop.getProperty(Utils.PAR_END_HEIGHT));
        assert startHeight == store.getChainHead().getHeight();
        Utils.log("IBD finished. Chain head " + store.getChainHead().getHeight() +  ", expected "+ startHeight);

        // Monitor replay
        final SettableFuture<Boolean> serverClosed = SettableFuture.create();
        chain.addNewBestBlockListener(new NewBestBlockListener() {
            @Override
            public void notifyNewBestBlock(StoredBlock block) throws VerificationException {
                Utils.log("Got block " + block.getHeight());

                // End replay condition
                if (block.getHeight() >= endHeight) {
                    serverClosed.set(Boolean.TRUE);
                }
            }
        });

        // Notify the replayer that the IBD is finished
        Message msg = new Ping(Long.valueOf(prop.getProperty(Utils.PAR_SYNC_NONCE)));
        srv.sendMessage(msg);
        Utils.log("Sent message to the proxy that we finished Initial Block Download.");

        if (!withReplay) {
            Utils.log("Server closing.");
            group.stop();
            store.close();
            return;
        }

        // Asynchronous block replay: blocks in [startHeight, endHeight]
        group.startBlockChainDownload(null);

        // Replay finished
        serverClosed.get();
        assert endHeight == store.getChainHead().getHeight();

        Utils.log("Server closed. Chain head " + store.getChainHead().getHeight() +  ", expected "+ endHeight);
        group.stop();
        store.close();
    }
}
