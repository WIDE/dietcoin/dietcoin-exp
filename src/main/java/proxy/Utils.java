package proxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerAddress;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.listeners.ChainDownloadStartedEventListener;
import org.bitcoinj.core.listeners.PeerConnectedEventListener;
import org.bitcoinj.core.listeners.PeerDisconnectedEventListener;
import org.bitcoinj.core.listeners.PeerDiscoveredEventListener;

public class Utils {
    // TODO: default values for parameters
    public static final String DEFAULT_PROPERTIES_FILENAME = "debug.properties";
    public static final String PAR_VERBOSE = "verbose";

    public static final String PAR_BTC_HOST = "btcHost";
    public static final String PAR_BTC_PORT = "btcPort";
    public static final String PAR_PROXY_HOST = "proxyHost";
    public static final String PAR_PROXY_PORT = "proxyPort";
    public static final String PAR_SYNC_NONCE = "syncNonce";

    public static final String PAR_HEADERS_DIR = "headersDir";
    public static final String PAR_BLOCK_INDEX_FILE = "blockIndexFile";
    public static final String PAR_REPLAYER_BTC_DIR = "replayerBtcDir";
    public static final String PAR_REPLAYER_DTC_DIR = "replayerDtcDir";
    public static final String PAR_MAX_STORED_BLOCKS = "maxStoredBlocks";
    public static final String PAR_NB_BITS_SHARDING = "bitsSharding";

    public static final String PAR_CLIENT_BTC_SPV_DIR = "clientBtcSpvDir";
    public static final String PAR_CLIENT_BTC_FULL_DIR = "clientBtcFullDir";
    public static final String PAR_CLIENT_DTC_DIET_DIR = "clientDtcDietDir";
    public static final String PAR_CLIENT_DTC_FULL_DIR = "clientDtcFullDir";
    public static final String PAR_CLIENT_MAX_STORED_BLOCKS = "clientMaxStoredBlocks";
    public static final String PAR_DIET_VERIFY_SUBCHAIN_LENGTH = "dietVerifySubchainLength";

    public static final String PAR_START_HEIGHT = "startHeight";
    public static final String PAR_END_HEIGHT = "endHeight";
    public static final String PAR_REPLAY_FREQ = "replayFreqMillis";


    public static void log(Object obj) {
        System.out.println((char) 27 + "[32m" + obj.toString() + (char) 27 + "[0m");
    }

    public static void debug(Object obj) {
        System.out.println((char) 27 + "[33m" + obj.toString() + (char) 27 + "[0m");
    }

    public static Properties loadProperties(String[] args) {
        if (args.length < 2) {
            log("No properties file specified, loading default file: " + DEFAULT_PROPERTIES_FILENAME);
            return Utils.loadProperties(DEFAULT_PROPERTIES_FILENAME);
        } else {
            return Utils.loadProperties(args[1]);
        }
    }

    public static Properties loadProperties(String filename) {
        File f = new File(filename);
        if (!f.exists() || !f.isFile()) {
            log("Error: this file doesn't exist or is not regular: " + filename);
            System.exit(2);
        }
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    public static LinkedList<Sha256Hash> readIndexFile(String indexFilename) {
        LinkedList<Sha256Hash> hashes = new LinkedList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(indexFilename));
            String line = reader.readLine(); // Ignore csv header

            line = reader.readLine();
            while (line != null) {
                String[] pair = line.split(","); // 0: height, 1: hash
                hashes.add(Sha256Hash.wrap(pair[1]));
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
        }
        return hashes;
    }

    public static void storeIndexFile(List<Sha256Hash> hashes, String indexFilename) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(indexFilename);
            StringBuilder sb = new StringBuilder();

            sb.append("block height, block hash\n");
            Iterator<Sha256Hash> it = hashes.iterator(); // because it's a linked list
            for (int i = 0; it.hasNext(); i++) {
                sb.append(i + "," + it.next().toString() + "\n");
            }

            writer.write(sb.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            if (writer != null)
                try {
                    writer.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
        }
    }

    // For debug only
    public static void addPeerGroupEvents(PeerGroup group) {
        group.addChainDownloadStartedEventListener(new ChainDownloadStartedEventListener() {
            @Override
            public void onChainDownloadStarted(Peer peer, int blocksLeft) {
                debug("chain dl started, left=" + blocksLeft + "    peer="+ peer);
            }
        });
        group.addConnectedEventListener(new PeerConnectedEventListener() {
            @Override
            public void onPeerConnected(Peer peer, int peerCount) {
                debug("connection, count=" + peerCount + "    peer=" + peer);
            }
        });
        group.addDisconnectedEventListener(new PeerDisconnectedEventListener() {
            @Override
            public void onPeerDisconnected(Peer peer, int peerCount) {
                debug("disconnection, count=" + peerCount + "    peer=" + peer);
                debug("trying to reconnect with peer=" + peer + " in 10s");
                try {
                    Thread.sleep(10*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                group.connectTo(peer.getAddress().getSocketAddress());
            }
        });
        group.addDiscoveredEventListener(new PeerDiscoveredEventListener() {
            @Override
            public void onPeersDiscovered(Set<PeerAddress> peerAddresses) {
                debug("peer discovered, size=" + peerAddresses.size());
            }
        });
    }

    // https://www.mkyong.com/java/how-to-copy-directory-in-java/
    public static void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
            }
            // Recursive copy of all directory files
            String files[] = src.list();
            for (String f : files) {
                File srcFile = new File(src, f);
                File destFile = new File(dest, f);
                copyFolder(srcFile, destFile);
            }
        } else {
            // Not directory => file (fingers crossed)
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[4096];
            int length;
            while ((length = in.read(buffer)) > 0){
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
        }
    }

    // Remove trailing slashes in directory name
    public static String cleanDirName(String dir) {
        return dir.replaceAll("/+$", "");
    }

    // Creates a new directory with the same name as prefix + the date as suffix
    public static String getUniqueDirName(String dir) throws IOException {
        dir = cleanDirName(dir);
        dir += "_"  + DateTimeFormatter.ISO_INSTANT.format(new Date().toInstant());
        Files.createDirectories(Paths.get(dir));
        return dir;
    }
}
