package proxy;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.bitcoinj.core.Block;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.FullPrunedBlockChain;
import org.bitcoinj.core.InventoryMessage;
import org.bitcoinj.core.Message;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerAddress;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Ping;
import org.bitcoinj.core.PrunedException;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.core.UTXOProviderException;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.core.listeners.NewBestBlockListener;
import org.bitcoinj.core.listeners.PreMessageReceivedEventListener;
import org.bitcoinj.net.NioServer;
import org.bitcoinj.net.StreamConnection;
import org.bitcoinj.net.StreamConnectionFactory;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.FullPrunedBlockStore;
import org.bitcoinj.store.LevelDBBlockStore;
import org.bitcoinj.store.LevelDBFullPrunedBlockStore;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.Wallet;

import com.google.common.util.concurrent.SettableFuture;

import dietcoin.LevelDBShardedBlockStore;

public class Replayer {
    public static void runReplayer(Properties prop, boolean isDietcoin, boolean withReplay) throws BlockStoreException, VerificationException, PrunedException,
    UTXOProviderException, InterruptedException, ExecutionException, IOException {
        boolean verbose = Boolean.valueOf(prop.getProperty(Utils.PAR_VERBOSE, "false"));
        int startHeight = Integer.valueOf(prop.getProperty(Utils.PAR_START_HEIGHT));
        int endHeight = Integer.valueOf(prop.getProperty(Utils.PAR_END_HEIGHT));

        // Reconstruct the list of header hashes from the existing hash index file
        LinkedList<Sha256Hash> hashes = Utils.readIndexFile(prop.getProperty(Utils.PAR_BLOCK_INDEX_FILE));
        if (verbose) Utils.debug("hashlist size=" + hashes.size() + "    first=" + hashes.getFirst() + "    last=" + hashes.getLast());

        NetworkParameters params = MainNetParams.get();
        Context context = new Context(params, true); // Second argument is for Dietcoin (isInReplay)
        Wallet wallet = new Wallet(params);

        // Copy the the bootstrapped chain to not have to reconstruct it
        String paramDir = isDietcoin ? Utils.PAR_REPLAYER_DTC_DIR : Utils.PAR_REPLAYER_BTC_DIR;
        String srcDir = Utils.cleanDirName(prop.getProperty(paramDir));
        String storeDir = Utils.getUniqueDirName(prop.getProperty(paramDir));
        Files.createDirectories(Paths.get(storeDir + "/..")); // Only create the parents of the directory
        Utils.log("Copying store base from " + srcDir + " to " + storeDir);
        Utils.copyFolder(new File(srcDir), new File(storeDir));

        // Open the copy of the full node store
        Utils.log("Bootstrapping copied chain " + storeDir);
        FullPrunedBlockStore fullStore = null;
        int nbBlocks = Integer.valueOf(prop.getProperty(Utils.PAR_CLIENT_MAX_STORED_BLOCKS));
        if (nbBlocks < 0) nbBlocks = Integer.MAX_VALUE;
        if (isDietcoin) {
            fullStore = new LevelDBShardedBlockStore(params, storeDir, nbBlocks, nbBlocks); // Store as many versioned shards as blocks
            int nbBitsSharding = Integer.valueOf(prop.getProperty(Utils.PAR_NB_BITS_SHARDING));
            ((LevelDBShardedBlockStore) fullStore).setShardingPolicy(nbBitsSharding);
        } else {
            fullStore = new LevelDBFullPrunedBlockStore(params, storeDir, nbBlocks);
        }
        FullPrunedBlockChain fullChain = new FullPrunedBlockChain(params, fullStore);
        fullChain.setRunScripts(false); // Disable transaction verification

        // Don't link the peer group with the full blockchain or it could download it automatically as the remote node
        // announces a new block, instead connect it to the already downloaded header chain
        BlockStore headerStore = new LevelDBBlockStore(context, new File(prop.getProperty(Utils.PAR_HEADERS_DIR)));
        BlockChain headerChain = new BlockChain(context, headerStore);
        PeerGroup group = new PeerGroup(context, headerChain);

        group.addWallet(wallet);
        group.setConnectTimeoutMillis(10*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);
        group.start();
        if (verbose) Utils.addPeerGroupEvents(group);

        // Connect to the BTC host for the replay
        Utils.log("Connecting to the remote bitcoin-core node to cherry-pick blocks to replay.");
        group.connectTo(new InetSocketAddress(prop.getProperty(Utils.PAR_BTC_HOST), Integer.valueOf(prop.getProperty(Utils.PAR_BTC_PORT))));
        group.waitForPeers(1).get();
        Peer remoteSrv = group.getDownloadPeer();

        // Open the replayer port to the outside if the client is not run locally (supposed the replayer and client read the same file)
        String thisHost = prop.getProperty(Utils.PAR_PROXY_HOST);
        if (!thisHost.equals("127.0.0.1") && !thisHost.equals("localhost")) {
            thisHost = "0.0.0.0";
        }

        // Allow client connections
        final SettableFuture<Boolean> isClientReady = SettableFuture.create();
        NioServer server = new NioServer(new StreamConnectionFactory() {
            @Override
            public StreamConnection getNewConnection(InetAddress inetAddress, int port) {
                // Connect the client to the replayed chain
                Peer cli = new Peer(params, fullChain, new PeerAddress(inetAddress, port), "Dietcoin replayer", "0.14", true);
                cli.setRemoteServer(remoteSrv);
                Utils.log("Client connected: " + inetAddress.getHostAddress() + ":" + port + ", starting IBD.");

                // Forward headers to the client
                // TODO: Also forward other push messages? All inv messages?
                fullChain.addNewBestBlockListener(new NewBestBlockListener() {
                    @Override
                    public void notifyNewBestBlock(StoredBlock block) throws VerificationException {
                        List<Block> list = new ArrayList<Block>();
                        list.add(block.getHeader());
                        cli.sendMessage(new InventoryMessage(params, list));
                    }
                });

                // Wait for the client to send the magic number before starting the replay
                cli.addPreMessageReceivedEventListener(Threading.SAME_THREAD, new PreMessageReceivedEventListener() {
                    @Override
                    public Message onPreMessageReceived(Peer peer, Message m) {
                        // From Peer.processPing()
                        if (m instanceof Ping) {
                            Ping p = (Ping) m;
                            if (p.hasNonce() && p.getNonce() == Long.valueOf(prop.getProperty(Utils.PAR_SYNC_NONCE))) {
                                isClientReady.set(Boolean.TRUE);
                            }
                        }
                        // Keep processing the message
                        return m;
                    }
                });
                return cli;
            }
        }, new InetSocketAddress(thisHost, Integer.valueOf(prop.getProperty(Utils.PAR_PROXY_PORT))));
        server.startAsync();
        server.awaitRunning();

        // Wait for client
        Utils.log("Waiting for a client to connect and finish its IBD.");
        isClientReady.get();
        Utils.log("Client IBD finished.");

        if (!withReplay) {
            Utils.log("Server closing.");
            remoteSrv.close();
            group.stop();
            fullStore.close();
            headerStore.close();

            server.stopAsync();
            server.awaitTerminated();

            Utils.log("Replayer data in " + storeDir + ", delete it maybe?");
            return;
        }

        // Replay blocks from startHeight+1 to endHeight, headers are forwarded to the client as if new blocks are broadcast
        int replayFreq = Integer.valueOf(prop.getProperty(Utils.PAR_REPLAY_FREQ));
        Utils.log("Starting replay of blocks [" + startHeight + ", " + endHeight + "] with a block every " + replayFreq + " ms.");
        int counter = 0;
        for (int height = startHeight; height < hashes.size() && height <= endHeight; height++) {
            Block b = remoteSrv.getBlock(hashes.get(height)).get();
            fullChain.add(b);
            counter++;
            System.out.print(".");
            if (counter % 50 == 0) System.out.println(" " + counter);
            Thread.sleep(replayFreq);
        }
        if (counter % 50 != 0) System.out.println(" " + counter);

        // Replay finished, closing shop
        assert endHeight == fullStore.getChainHeadHeight();
        Sha256Hash bestHash = fullStore.getChainHead().getHeader().getHash();
        Sha256Hash bestVerifiedHash = fullStore.getVerifiedChainHead().getHeader().getHash();
        assert bestVerifiedHash.equals(bestHash);
        Utils.log("Replay finished at block: " + endHeight);
        if (verbose) Utils.debug("Best          block height=" + fullChain.getBestChainHeight() + "    hash=" + fullChain.getChainHead().getHeader().getHash());
        if (verbose) Utils.debug("Best verified block height=" + fullStore.getVerifiedChainHead().getHeight() + "    hash=" + fullStore.getVerifiedChainHead().getHeader().getHashAsString());

        remoteSrv.close();
        group.stop();
        fullStore.close();
        headerStore.close();

        server.stopAsync();
        server.awaitTerminated();

        Utils.log("Replayer data in " + storeDir + ", delete it maybe?");
    }
}
