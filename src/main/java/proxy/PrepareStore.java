package proxy;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Block;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.FullPrunedBlockChain;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Peer;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.PrunedException;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.UTXOProviderException;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.FullPrunedBlockStore;
import org.bitcoinj.store.LevelDBBlockStore;
import org.bitcoinj.store.LevelDBFullPrunedBlockStore;
import org.bitcoinj.wallet.Wallet;

import dietcoin.LevelDBShardedBlockStore;

public class PrepareStore {
    public static void prepareStore(Properties prop, boolean isDietcoin) throws BlockStoreException, VerificationException, PrunedException,
    UTXOProviderException, InterruptedException, ExecutionException, IOException {
        boolean verbose = Boolean.valueOf(prop.getProperty(Utils.PAR_VERBOSE, "false"));
        int versioningStartHeight = -1;

        // Reconstruct the list of header hashes from the existing hash index file
        LinkedList<Sha256Hash> hashes = Utils.readIndexFile(prop.getProperty(Utils.PAR_BLOCK_INDEX_FILE));
        if (verbose) Utils.debug("hashlist size=" + hashes.size() + "    first=" + hashes.getFirst() + "    last=" + hashes.getLast());

        NetworkParameters params = MainNetParams.get();
        Context context = Context.getOrCreate(params);
        Wallet wallet = new Wallet(params);

        // Prepare for a full node store (blocks + UTXO set)
        int nbBlocks = Integer.valueOf(prop.getProperty(Utils.PAR_MAX_STORED_BLOCKS));
        if (nbBlocks < 0) nbBlocks = Integer.MAX_VALUE;
        String storeDir = isDietcoin ? prop.getProperty(Utils.PAR_REPLAYER_DTC_DIR) : prop.getProperty(Utils.PAR_REPLAYER_BTC_DIR);
        Files.createDirectories(Paths.get(storeDir));
        FullPrunedBlockStore store;
        if (isDietcoin) {
            store = new LevelDBShardedBlockStore(params, storeDir, nbBlocks, 0); // 0 = no versioning, value set below
            int nbBitsSharding = Integer.valueOf(prop.getProperty(Utils.PAR_NB_BITS_SHARDING));
            ((LevelDBShardedBlockStore) store).setShardingPolicy(nbBitsSharding);

            // Start versioning only when needed, or right away if we're resuming this prepare method
            versioningStartHeight = Integer.valueOf(prop.getProperty(Utils.PAR_START_HEIGHT)) - nbBlocks - 10; // Extra 10
            if (versioningStartHeight < 0) versioningStartHeight = 0;
            if (versioningStartHeight <= store.getChainHeadHeight()) {
                ((LevelDBShardedBlockStore) store).setVersioningMaxAge(nbBlocks);
            }
            Utils.log("Versioning is activated at block height " + versioningStartHeight);
        } else {
            store = new LevelDBFullPrunedBlockStore(params, storeDir, nbBlocks);
        }
        FullPrunedBlockChain chain = new FullPrunedBlockChain(params, store);
        chain.setRunScripts(false); // Disable transaction verification

        // Don't link the peer group with the full blockchain or it could download it automatically as the remote node
        // announces a new block, instead connect it to the already downloaded header chain
        BlockStore headerStore = new LevelDBBlockStore(context, new File(prop.getProperty(Utils.PAR_HEADERS_DIR)));
        BlockChain headerChain = new BlockChain(context, headerStore);
        PeerGroup group = new PeerGroup(context, headerChain);

        group.addWallet(wallet);
        group.setConnectTimeoutMillis(2*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);
        group.start();
        if (verbose) Utils.addPeerGroupEvents(group);

        // Connect to the bitcoin-core full node to get blocks one by one
        group.connectTo(new InetSocketAddress(prop.getProperty(Utils.PAR_BTC_HOST), Integer.valueOf(prop.getProperty(Utils.PAR_BTC_PORT))));
        group.waitForPeers(1).get();
        Peer remoteSrv = group.getDownloadPeer();

        // Construct the full node store up to a given block, the process can be stopped and resumed
        int fractionDisplay = 20;
        long heartbeatDelay = 2 * 60 * 1000;
        long stamp = System.currentTimeMillis();
        int startHeight = store.getChainHeadHeight() + 1;
        int endHeight = Integer.valueOf(prop.getProperty(Utils.PAR_START_HEIGHT)) - 1;
        if (startHeight <= endHeight) {
            Utils.log("Downloading blocks [" + startHeight + ", " + endHeight + "] one by one from " + prop.getProperty(Utils.PAR_BTC_HOST));
            for (int height = startHeight; height < hashes.size() && height <= endHeight; height++) {
                if (isDietcoin && versioningStartHeight == height) {
                    ((LevelDBShardedBlockStore) store).setVersioningMaxAge(Integer.valueOf(prop.getProperty(Utils.PAR_MAX_STORED_BLOCKS)));
                    Utils.log("Activating versioning at block height " + height);
                }

                Block b = remoteSrv.getBlock(hashes.get(height)).get();
                chain.add(b);
                // Print the downloaded block count every X% of progress
                if ((height - startHeight + 1) % ((endHeight - startHeight + 1) / fractionDisplay) == 0) {
                    String progress = String.format(Locale.US, "%.2f", 100.0 * (height - startHeight + 1) / (endHeight - startHeight + 1));
                    Utils.log("Downloaded " + progress + "% of the remaining chain (" + height + "/" + endHeight + " total blocks)");
                }
                // Regularly print a hearbeat
                long currentStamp = System.currentTimeMillis();
                if (currentStamp - stamp > heartbeatDelay) {
                    stamp = currentStamp;
                    Utils.debug("Still processing the chain, current block height: " + height + ", next heartbeat in " + heartbeatDelay/1000 + "s");
                }
            }
        }
        assert endHeight == store.getChainHeadHeight();
        Sha256Hash bestHash = store.getChainHead().getHeader().getHash();
        Sha256Hash bestVerifiedHash = store.getVerifiedChainHead().getHeader().getHash();
        assert bestVerifiedHash.equals(bestHash);
        Utils.log("Full node store populated with blocks [0, " + endHeight + "] in directory: " + prop.getProperty(Utils.PAR_REPLAYER_BTC_DIR));
        if (verbose) Utils.debug("Best          block height=" + chain.getBestChainHeight() + "    hash=" + chain.getChainHead().getHeader().getHash());
        if (verbose) Utils.debug("Best verified block height=" + store.getVerifiedChainHead().getHeight() + "    hash=" + store.getVerifiedChainHead().getHeader().getHashAsString());

        remoteSrv.close();
        group.stop();
        store.close();
        headerStore.close();
    }
}
