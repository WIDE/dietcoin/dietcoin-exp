package proxy;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Context;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.StoredBlock;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.store.BlockStore;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.LevelDBBlockStore;
import org.bitcoinj.wallet.Wallet;

public class PrepareIndex {
    public static void prepareIndex(Properties prop) throws BlockStoreException, InterruptedException, ExecutionException, IOException {
        boolean verbose = Boolean.valueOf(prop.getProperty(Utils.PAR_VERBOSE, "false"));

        NetworkParameters params = MainNetParams.get();
        Context context = Context.getOrCreate(params);
        Wallet wallet = new Wallet(params);

        // Store the headers on disk, saves time when debugging
        Files.createDirectories(Paths.get(prop.getProperty(Utils.PAR_HEADERS_DIR)));
        BlockStore headerStore = new LevelDBBlockStore(context, new File(prop.getProperty(Utils.PAR_HEADERS_DIR)));
        BlockChain headerChain = new BlockChain(context, headerStore);
        PeerGroup group = new PeerGroup(context, headerChain);

        group.addWallet(wallet);
        group.setConnectTimeoutMillis(2*1000);
        group.disablePeerDiscovery();
        group.setPeerDiscoveryTimeoutMillis(0);
        group.setMaxPeersToDiscoverCount(0);
        group.setMaxConnections(5);
        group.start();
        if (verbose) Utils.addPeerGroupEvents(group);

        // Connect to the bitcoin-core full node and download the headers
        group.connectTo(new InetSocketAddress(prop.getProperty(Utils.PAR_BTC_HOST), Integer.valueOf(prop.getProperty(Utils.PAR_BTC_PORT))));
        group.waitForPeers(1).get();
        Utils.log("Downloading the up-to-date chain of headers from " + prop.getProperty(Utils.PAR_BTC_HOST));
        group.downloadBlockChain(); // Counter-intuitive: it only gets the headers

        // Construct the index <block height, block hash> by parsing the chain backward from the tip to the genesis block (excluded)
        Utils.log("Parsing the received chain to create the block index.");
        LinkedList<Sha256Hash> hashes = new LinkedList<>();
        int firstHeight = Integer.MAX_VALUE;
        int lastHeight = Integer.MIN_VALUE;
        StoredBlock sb = headerStore.getChainHead();
        while (sb != null) {
            hashes.addFirst(sb.getHeader().getHash());
            if (firstHeight > sb.getHeight()) firstHeight = sb.getHeight();
            if (lastHeight < sb.getHeight()) lastHeight = sb.getHeight();
            sb = sb.getPrev(headerStore);
        }
        assert firstHeight == 0;
        Utils.storeIndexFile(hashes, prop.getProperty(Utils.PAR_BLOCK_INDEX_FILE));
        Utils.log("Headers cached in: " + prop.getProperty(Utils.PAR_HEADERS_DIR));
        Utils.log("Block index file populated up to block " + lastHeight +  " in: " + prop.getProperty(Utils.PAR_BLOCK_INDEX_FILE));
        if (verbose) Utils.debug("hashlist size=" + hashes.size() + "    first=" + firstHeight + "    last=" + lastHeight);
        if (verbose) Utils.debug(hashes.getFirst() + "     " + hashes.getLast());

        group.stop();
        headerStore.close();
    }
}
