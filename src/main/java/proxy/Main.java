package proxy;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.bitcoinj.core.PrunedException;
import org.bitcoinj.core.UTXOProviderException;
import org.bitcoinj.core.VerificationException;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.utils.BriefLogFormatter;

public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            printUsage();
            return;
        }

        // Produces more compact and more readable log, especially when using the JDK log adapter.
        BriefLogFormatter.init();
        // Load properties
        Properties prop = Utils.loadProperties(args);

        // TODO: log.print: starting class with params x y z

        // Desactivate replay ability for dietcoin (temporary fix)
        String main = args[0].toLowerCase();
        boolean withReplay = ! (main.equals("replayerdtc") || main.equals("clientdtcfull"));
        try {
            switch (main) {
            case "prepareindex":
                PrepareIndex.prepareIndex(prop);
                break;

            /** Bitcoin **/
            case "preparestorebtc":
                PrepareStore.prepareStore(prop, false);
                break;
            case "replayerbtc":
                Replayer.runReplayer(prop, false, withReplay);
                break;
            case "clientbtcspv":
                Client.runClient(prop, Client.MODE_BTC_SPV, withReplay);
                break;
            case "clientbtcfull":
                Client.runClient(prop, Client.MODE_BTC_FULL, withReplay);
                break;

            /** Dietcoin */
            case "preparestoredtc":
                PrepareStore.prepareStore(prop, true);
                break;
            case "replayerdtc":
                Replayer.runReplayer(prop, true, withReplay);
                break;
            case "clientdtcdiet":
                System.out.println("Unimplemented feature.");
                break;
            case "clientdtcfull":
                Client.runClient(prop, Client.MODE_DTC_FULL, withReplay);
                break;
            default:
                printUsage();
                return;
            }
        } catch (BlockStoreException | VerificationException | PrunedException | UTXOProviderException |
                InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void printUsage() {
        System.out.println("Usage: dietcoin-exp STEP [PROPERTY-FILE]\n"
                + "\n"
                + "    STEP             prepareIndex\n"
                + "                     prepareStoreBtc replayerBtc clientBtcFull clientBtcSpv\n"
                + "                     prepareStoreDtc replayerDtc clientDtcFull\n"
                + "    PROPERTY-FILE    Path to the experiment config file (default: debug.properties)");
    }

}

