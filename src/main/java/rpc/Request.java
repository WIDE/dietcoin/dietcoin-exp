package rpc;

import java.util.List;

public class Request {
    public String id;
    public String method;
    public List<Object> params;

    public Request(String method, List<Object> params, String id) {
        this.method = method;
        this.params = params;
        this.id = id;
    }

    public Request() {
        this("", null, "BoatyMcBoatFace");
    }

    @Override
    public String toString() {
        return "method: " + method + "\tparams: " + params.toString() + "\tid: " + id;
    }

}
