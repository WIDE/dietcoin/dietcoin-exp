package rpc;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;

import net.iharder.Base64;

// Singleton
public enum RPCClient {
    INSTANCE;

    private static Gson gson;
    private static URL url;
    private static String encoding;
    private static String id;

    public static void init(Properties prop) {
        gson = new Gson();

        encoding = prop.getProperty("rpcuser") + ":" + prop.getProperty("rpcpassword");
        encoding = Base64.encodeBytes(encoding.getBytes());
        try {
            url = new URL("http://" + prop.getProperty("rpchost") + ":" + prop.getProperty("rpcport"));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        id = prop.getProperty("rpcid", "whocares");
    }

    public static Reply doRequest(Request rq) {
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/plain");
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.getOutputStream().write(gson.toJson(rq).getBytes());
            conn.getOutputStream().flush();
            conn.getOutputStream().close();

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP " + conn.getResponseCode() + " " + conn.getResponseMessage());
            }
            return gson.fromJson(new InputStreamReader(conn.getInputStream()), Reply.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static int getBlockCount() {
        Reply rep = doRequest(new Request("getblockcount", null, id));
        return ((Double) rep.result).intValue();
    }

    public static String getBlockHash(int height) {
        List<Object> params = new ArrayList<>();
        params.add(height);
        Reply rep = doRequest(new Request("getblockhash", params, id));
        return rep.result.toString();
    }

    public static String getBlockHeader(String hash, boolean isInJsonForm) {
        List<Object> params = new ArrayList<>();
        params.add(hash);
        params.add(isInJsonForm);
        Reply rep = doRequest(new Request("getblockheader", params, id));
        return rep.result.toString();
    }

    public static String getBlock(String hash, boolean isInJsonForm) {
        List<Object> params = new ArrayList<>();
        params.add(hash);
        params.add(isInJsonForm);
        Reply rep = doRequest(new Request("getblock", params, id));
        return rep.result.toString();
    }
}
