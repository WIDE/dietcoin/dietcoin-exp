package rpc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MainRPC {
    private static Properties prop;

    private static long startTime, endTime;

    public static void main(String[] args) {
        try {
            init(args);

            System.out.println("===== BLOCK COUNT");
            int blockCount = RPCClient.getBlockCount();
            System.out.println(blockCount);
            blockCount = 100000;

            System.out.println("===== HASHES");
            List<String> blockHashes = new ArrayList<>();
            for (int height = 0; height <= blockCount; height++) {
                blockHashes.add(RPCClient.getBlockHash(height));
                if (height % 10000 == 0) {
                    System.out.println("" + height + "\t" + blockHashes.get(height));
                }
            }

            System.out.println("===== BLOCKS");
            long size = 0;
            Map<String, String> blockHeaders = new HashMap<>();
            StringBuffer allHeaders = new StringBuffer();
            for (int height = 0; height < blockCount; height++) {
                String hash = blockHashes.get(height);
//                blockHeaders.put(hash, RPCClient.getBlock(hash, false));
//                allHeaders.append(blockHeaders.get(hash));
                size += RPCClient.getBlock(hash, false).length()/2;
                if (height % 10000 == 0) {
                    System.out.println("" + height + "\t" + size / 1024 / 1024 + " Mb");
                }
            }

            double nbBytes = size;
            System.out.println("Blocks list total size: " + (nbBytes / 1024 / 1024) + " Mb");

            end();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static void init(String[] args) {
        startTime = System.currentTimeMillis();

        // Check program argument
        if (args.length < 1) {
            System.err.println("Missing the config file as argument.");
            System.exit(1);
        }
        File f = new File(args[0]);
        if (!f.exists() || !f.isFile()) {
            System.err.println("The argument \"" + args[0] + "\" doesn't exist or isn't a regular file.");
            System.exit(2);
        }

        // TODO: default properties with host and port, but without user and password

        // Load properties
        prop = new Properties();
        try {
            prop.load(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        RPCClient.init(prop);
    }

    public static void end() {
        endTime = System.currentTimeMillis();
        System.out.println("===== END");
        System.out.println("Wall-clock time: " + ((endTime - startTime)) + " ms");
    }

}

