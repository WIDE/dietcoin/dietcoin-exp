package rpc;

public class Reply {
    public Object result;
    public Object error;
    public String id;

    @Override
    public String toString() {
        return "result: " + result + "\terror: " + error + "\tid: " + id;
    }
}
